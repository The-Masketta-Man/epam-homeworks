import org.junit.Assert;
import org.junit.Test;

public class MainTest {
    @Test
    public void testQuadrantI(){
        Assert.assertEquals("I", Main.findQuadrant(0.0000004, 123456.7));
    }
    @Test
    public void testQuadrantII(){
        Assert.assertEquals("II", Main.findQuadrant(-0.254, 0.333));
    }
    @Test
    public void testQuadrantIII(){
        Assert.assertEquals("III", Main.findQuadrant(-444.4, -1));
    }
    @Test
    public void testQuadrantIV(){
        Assert.assertEquals("IV", Main.findQuadrant(3.5, -3.6));
    }
    @Test
    public void testXAxis(){
        Assert.assertEquals("x axis", Main.findQuadrant(5.55, 0));
    }
    @Test
    public void testYAxis(){
        Assert.assertEquals("y axis", Main.findQuadrant(0, -3.45));
    }
    @Test
    public void testOriginPoint(){
        Assert.assertEquals("origin point", Main.findQuadrant(0, -0));
    }
}
