import java.util.Locale;
import java.util.Scanner;

public class Main {
    static String findQuadrant(double x, double y){
        int quadrant = (int) Math.signum(x) + (int) Math.signum(y) * 3;
        switch (quadrant){
            case (0):
                return "origin point";
            case (1):
            case (-1):
                return "x axis";
            case (3):
            case (-3):
                return "y axis";
            case (4):
                return "I";
            case (2):
                return "II";
            case (-4):
                return "III";
            case (-2):
                return "IV";
        }
        return "impossible point!";
    }

    public static void main(String[] args) {
            Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
            double x = scanner.nextDouble();
            double y = scanner.nextDouble();
            System.out.println(findQuadrant(x, y));
    }
}
